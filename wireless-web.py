import http.server
import subprocess
import time
import sys
import re
import os

class MyHTTPRequestHandler(http.server.BaseHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

        def do_GET(self, *args, **kwargs):
                print(wificontroller)
                try:
                        cssidmatch = re.match("^/switch\?cssid=(?P<cssid>[0-9]+)$", self.path)
                        autoswitch = re.match("^/autoswitch\?state=(?P<state>enable|disable)$", self.path)
                        if self.path == "/":
                                out = subprocess.run("%s scan"%(wificontroller),shell=True, capture_output=True)
                                self.send_response(200)
                                self.end_headers()
                                html = "<html><head><title>sync-wireless</title></head><body><h4>Available Networks:</h4><ul>"
                                ssids = out.stdout.decode("utf-8").strip().split('\n')
                                print(ssids)
                                for ssid in ssids:
                                        html += "<li>" + ssid + "</li>"
                                html += '</ul><h4>Configured Networks:</h4><form action="/switch">'
                                out = subprocess.run("%s list"%(wificontroller), shell=True, capture_output=True)
                                cssids = out.stdout.decode("utf-8").strip().split('\n')
                                for cssid in cssids:
                                        cssidsplit = cssid.split(",")
                                        priority=cssidsplit[0]
                                        name=cssidsplit[1]
                                        iscurrent=cssidsplit[2]
                                        html += '<input type="radio" name="cssid" value="' + priority + '"'
                                        if iscurrent == "1":
                                                html += " checked"
                                        html += ">" + name + "</input><br>"
                                html += '<input type="submit" value="Switch"><h4>Autoswitching</h4></form><form action="/autoswitch">'
                                autoswitch = True
                                out = subprocess.run("%s isautoswitch"%(wificontroller), shell=True, capture_output=True)
                                autoswitch = out.stdout.decode("utf-8").strip() == "True"
                                html += '<input type="radio" name="state" value="enable"'
                                if autoswitch:
                                        html += " checked"
                                html += '>enabled (enable)</input><br>'
                                html += '<input type="radio" name="state" value="disable"'
                                if not autoswitch:
                                        html += " checked"
                                html += '>disabled (disable)</input><br>'
                                html += '<input type="submit" value="Switch"></form></body></html>'
                                self.wfile.write(html.encode("utf-8"))
                        elif cssidmatch is not None:
                                subprocess.run("%s switch "%(wificontroller) + cssidmatch['cssid'], shell=True)
                                self.send_response(302)
                                self.send_header("Location", "/")
                                self.end_headers()
                        elif autoswitch is not None:
                                subprocess.run("%s autoswitch "%(wificontroller) + autoswitch['state'], shell=True)
                                self.send_response(302)
                                self.send_header("Location", "/")
                                self.end_headers()
                        else:
                                self.send_response(404)
                                self.end_headers()
                except Exception as e:
                        print(e)
                        self.send_response(500)
                        self.end_headers()


if __name__ == "__main__":
        me = os.path.abspath(sys.argv[0])
        default_wificontroller=os.path.basename(me).replace('.py', '.sh')
        wificontroller = sys.argv[1] if len(sys.argv) > 1 else os.path.join(os.path.dirname(me), default_wificontroller)
        server = http.server.ThreadingHTTPServer(('',7777), MyHTTPRequestHandler)
        server.serve_forever()
