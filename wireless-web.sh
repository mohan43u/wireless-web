#!/bin/bash
set -x

sync_wireless_scan() {
    (wpa_cli scan &>/dev/null ; sleep 5; wpa_cli scan_results | sed '1,2d' | awk '{print $5}')
}

sync_wireless_list() {
    wpa_cli list_networks | sed '1,2d' | awk '{iscurrent=0; if($4 ~ /^\[CURRENT\]$/){ iscurrent=1; }; print $1","$2","iscurrent; }'
}

sync_wireless_switch() {
    wpa_cli select_network "${1}" >/dev/null
    channelmap=$((wpa_cli scan >/dev/null; sleep 5; wpa_cli scan_results) | sed '1,2d' | awk '{channel=($2 - 2407)/5; print channel","$5}')
    currentssid=$(wpa_cli list_networks | sed '1,2d' | awk '$4 ~ /^\[CURRENT\]/{print $2}')
    currentchannel=$(echo "${channelmap}" | grep "${currentssid}$" | cut -d',' -f1)
    hostapdconf=$(dirname $(readlink -f "${0}"))/hostapd.conf
    sed 's/^channel=.*$/channel='${currentchannel}'/g' "${hostapdconf}" | sudo tee /etc/hostapd/hostapd.conf > /dev/null
    sudo systemctl restart hostapd
}

sync_wireless_isautoswitch() {
    if (wpa_cli list_networks | sed '1,2d' | grep 'DISABLED' &>/dev/null); then
	echo "False"
    else
	echo "True"
    fi
}

sync_wireless_autoswitch() {
    if [[ "${1}" == "enable" ]]; then
	wpa_cli list_networks | sed '1,2d' | awk '{print "wpa_cli enable_network "$1; }' | bash
    else
	wpa_cli list_networks | sed '1,2d' | awk '{decision="disable"; if($4 ~ /^\[CURRENT\]$/){ decision="enable"; }; print "wpa_cli "decision"_network "$1; }' | bash
    fi
}

sync_wireless_${@}
